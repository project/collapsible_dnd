/**
 * @file
 * Provide better dragging capabilities to admin uis.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.collapsibleDnd = Drupal.collapsibleDnd || {};
  Drupal.collapsibleDnd.makeDraggablesBetter = function (context, settings) {
    if ($('body').hasClass('collapsible-dnd--processes')) {
      return;
    }
    $('body').addClass('collapsible-dnd--processes');
    // Create collapse handlers on load
    Drupal.collapsibleDnd.loopThroughTable(context, 'onload');
    // On click event.
    $('.draggables-collapser', context).click(function () {
      // Get the children.
      var $parent = $(this).parents('tr.draggable');
      var childRows = Drupal.collapsibleDnd.findChildren($parent);
      // Get next level to collapse/expand only the level below the clicked item
      var nxtLvl = parseInt($parent.data('lvl')) + 1;
      if ($(this).hasClass('state-expanded')) {
        // Update the status of this collapser.
        $(this).removeClass('state-expanded').addClass('state-collapsed');
        // Loop through <td>'s of child rows (only one level down) and collapse.
        $('td[data-lvl="' + nxtLvl + '"]', childRows).each(function () {
          // Hide and add the ID of the collapser.
          $(this).addClass('collapsible-dnd--hidden-td');
        });
      } else {
        // Update the status of this collapser.
        $(this).removeClass('state-collapsed').addClass('state-expanded');
        // Loop through <td>'s of child rows (only one level down) and expand.
        $('td[data-lvl="' + nxtLvl + '"]', childRows).each(function () {
          // Show and remove the ID of the collapser.
          $(this).removeClass('collapsible-dnd--hidden-td');
        });
      }
    });
    // On order change recalculate children and set collapse handlers
    // doing this by looking at the body class change, I couldn't figure
    // out a way to do this on mouseup.
    var $div = $("body");
    var config = {attributes: true};
    $div.each(function () {
      var target = this;
      var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
          // Recalculate collapse handlers after drag
          Drupal.collapsibleDnd.loopThroughTable('.draggable-table', 'dragged');
          // Hide when dragged is placed in collapsed parent
          var $draggedItem = $('.drag-previous');
          // Get dragged item level
          var draggedItemLvl = $draggedItem.data('lvl');
          // Get parent of dragged item after dropping it
          var $draggedParent = $draggedItem.prev('tr.draggable');
          // Get parents level
          var draggedParentLvl = $draggedParent.data('lvl');
          // if parent is collapsed and is one level above hide child
          if ($('>td>.tabledrag-cell-content>.draggables-collapser', $draggedParent).hasClass('state-collapsed') && (parseInt(draggedParentLvl + 1)) === parseInt(draggedItemLvl)) {
            $draggedItem.fadeOut(1000, function () {
              $('td', $draggedItem).addClass('collapsible-dnd--hidden-td');
              $draggedItem.show();
            });
          }
        });
      });
      observer.observe(target, config);
    });
  };
  Drupal.collapsibleDnd.loopThroughTable = function (tableElement, action) {
    if (action === 'onload') {
      // Create collapse handlers and make everything start in collapsed state
      $('a.tabledrag-handle', tableElement).each(function () {
        var collapser = $('<div class="draggables-collapser state-collapsed"></div>');
        $(collapser).insertBefore(this);
        var childRows = Drupal.collapsibleDnd.findChildren($(this).parents('tr.draggable'));
        if (childRows.length === 0) {
          $(collapser).addClass('state-has-no-children');
        } else {
          // Loop through <td>'s of child rows.
          $('td', childRows).each(function () {
            // Hide and add the ID of the collapser.
            $(this).addClass('collapsible-dnd--hidden-td');
          });
        }
        collapserId++;
      });
    } else if (action === 'dragged') {
      var collapserId = 0;
      $('a.tabledrag-handle', tableElement).each(function () {
        var childRows = Drupal.collapsibleDnd.findChildren($(this).parents('tr.draggable'));
        if (childRows.length === 0) {
          $(this).prev().addClass('state-has-no-children');
        } else {
          $(this).prev().removeClass('state-has-no-children');
        }
        collapserId++;
      });
    }
  }
  Drupal.collapsibleDnd.findChildren = function (rowElement) {
    var rows = [];
    var indentation = $(rowElement).find('.js-indentation').length;
    var currentRow = $(rowElement).next('tr.draggable');
    // Add data lvl to the tr and td to understand where they are in the tree
    rowElement.attr('data-lvl', indentation);
    $('>td', rowElement).attr('data-lvl', indentation);
    while (currentRow.length) {
      // A greater indentation indicates this is a child.
      if (currentRow.find('.js-indentation').length > indentation) {
        rows.push(currentRow[0]);
      } else {
        break;
      }
      currentRow = currentRow.next('tr.draggable');
    }
    return rows;
  };
  /**
   * Collapsible drag'n'drop draggables.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.collapsibleDnd = {
    attach: function (context, settings) {
      Drupal.collapsibleDnd.makeDraggablesBetter(context, settings);
    }
  };
})(jQuery, Drupal, drupalSettings);
