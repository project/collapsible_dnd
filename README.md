## Collapsible Drag 'n Drop

Managing drupal's menus relies heavily on draggable tables. Dragging
menu items around to order them and manage their hierarchy is very
intuitive.

But this system breaks down when your menu structure starts getting
more and more elaborate, which makes it difficult to drag parent items
with many children. It becomes almost impossible to handle an item if
its sub-tree is bigger than your screen!

This module makes it possible to collapse these sub-trees to make the
drag 'n drop action easy and intuitive again.

This doesn't only apply to draggable tables for menus, but to any
draggable tables.

### Requirements

No requirements at this time.

### Setup/Usage

* Install module like any other contributed module.
* Any draggable tables should now have the collapsible functionality
applied.

### Roadmap

* Be able to configure the areas this functionality should get applied
to.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
